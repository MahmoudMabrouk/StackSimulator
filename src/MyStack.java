
import java.util.ArrayList;

public class MyStack {

    private ArrayList<Integer> list  ;
    int sp = -1 ;
    int size ;

    public MyStack(int capacity ) {
        list = new ArrayList<>(capacity) ;
        size =capacity ;
    }

    public Integer peek ()
    {
        return list.get(sp) ;
    }

    public void push(Integer e ){
       ++ sp ;
        list.add( sp,e );
    }
    public Integer pop(){
        Integer e =list.get(sp) ;
        list.remove(sp) ;
        sp-- ;
        return  e ;
    }
    public boolean isEmpty ()
    {
        return sp == -1 ;
    }
    public boolean isFull (){
        return  sp == size ;
    }
    public int  search (Integer e )
    {
        int index =-1;
        for (int i = sp; i >= 0 ; i-- ) { //search from sp to bottom
            if ((list.get(i) == e )) {
                index = i;
                break;
            }
        }

        return  index > -1 ? sp-index  : -1   ;
    }
    public int getSize (){
        return  list.size() ;
    }
    public String dispaly()
    {
        StringBuilder sb = new StringBuilder() ;

        for (int i = sp; i >=  0 ; i--) {
                sb.append( list.get(i)) ;
                sb.append("\n");
        }
        return  sb.toString() ;

    }

}
