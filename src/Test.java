import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class Test extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    Label lS , lSize ;
    String slSP = "SP  is  " ;
    String sSize = "Size   is  " ;
    MyStack m = new MyStack(16) ;
    @Override
    public void start(Stage primaryStage) {


        Button pop = new Button("pop") ;
        Button push = new Button("push") ;
        Button search = new Button("Search") ;
     //   Button display = new Button("display") ;

        lS = new Label() ;
        lSize = new Label() ;
        setLabels();

        TextField tf = new TextField() ;
        tf.setPromptText("ادخل قيمة لادخالها في ال المكدس");
        tf.setMinWidth(50);
        TextArea ta =new TextArea() ;
        ta.setEditable(true);

        ta.setMaxWidth(50);


        GridPane g = new GridPane() ;
        g.setPadding(new Insets(20));
        g.setVgap(10);
        g.setHgap(10);
        g.setAlignment(Pos.CENTER);

        //Actions
        pop.setOnAction(e->{
            if (!m.isEmpty())
            {
                Integer r =m.pop() ;
                tf.setText(String.valueOf(r));
                setLabels();
                ta.setText(m.dispaly());
            }
            else
            {
                Alert.al("دا  فاضي يا معلم حاول مرة اخري ");
            }
        }
        );
        push.setOnAction(e->{
            if (!m.isFull())
            {
            String t = tf.getText();
           // tf.clear();
            try {
                Integer d = Integer.parseInt(t);
                m.push(d);
                setLabels();
                String  r = m.dispaly() ;
                System.out.println(r);
                ta.setText(r);

            }catch (NumberFormatException ex )
            {
                Alert.al("بجد انت بتهزر -- يا عم دخل ارقام وريحني بقي ") ;
            }

            }
            else
            {
                Alert.al("دا  مليان على اخره  يا معلم حاول مرة اخري ");
            }
        }
        );
        tf.setOnMousePressed(e->{ tf.clear();});

        search.setOnAction(e->{
            String tValue = tf.getText() ;
           // tf.clear();
            int x  ;
            try {
                Integer d = Integer.parseInt(tValue);
                x = m.search(d);
                if (x == -1 )
                {
                    Alert.al("للاسف القيمة مش موجودة ");
                }
                else {
                    tf.setText("pos   " + x + " after sp ");

                }




            }catch (NumberFormatException ex )
            {
                Alert.al("بجد انت بتهزر -- يا عم دخل ارقام وريحني بقي ") ;
            }

        });

       // g.addRow(0 , pop , push , search , display ,lS);
        g.addRow(0 , pop , push , search  ,lS);
        g.add( tf ,0 ,1 );
        g.addColumn(3 , lSize , ta);

        //g.setPrefWidth(primaryStage.getWidth()*0.8);

        primaryStage.setScene(new Scene(g));
        primaryStage.setTitle("MyStack");
        primaryStage.show();

    }
    public void setLabels ()
    {
        lS.setText(slSP +m.sp );
        lSize.setText(sSize +m.getSize() );
    }
}
